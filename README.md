```sh
ffmpeg -f pulse -i alsa_output.pci-0000_00_1f.3.hdmi-stereo.monitor -ac 2 -f x11grab -s 1920x1080 -i :0.0+1920,0 -vcodec libx264 -threads 0 -pix_fmt yuv420p -qp 0 -preset ultrafast -y /home/tr/Videos/Maus_2022-06-05.mkv

ffmpeg -i Maus_2022-06-19.mkv -c:v mpeg2video -c:a ac3 -f dvd -s 960x540 -r 25 -pix_fmt yuv420p -g 15 -b:v 4100k -maxrate 8000000 -minrate 0 -bufsize 1835008 -packetsize 2048 -muxrate 10080000 -b:a 448000 -ar 48000 -y Maus_2022-06-19.mpg

ffmpeg -f pulse -i alsa_output.pci-0000_00_1f.3.hdmi-stereo.monitor -ac 2 -f x11grab -s 1920x1080 -i :0.0+1920,0 -c:v mpeg2video -c:a ac3 -f dvd -s 960x540 -r 25 -pix_fmt yuv420p -g 15 -b:v 4100k -maxrate 8000000 -minrate 0 -bufsize 1835008 -packetsize 2048 -muxrate 10080000 -b:a 448000 -ar 48000 -y /home/tr/Videos/Maus_2022-07-10.mpg
```
### 2024-07-07
Lach- und Sachgeschichten in dieser Woche mit Clarissa und dem Geheimnis des Lupinen-Schnitzels, dem Märchen von einem mutigen Löwenjungen, André und einem sehr alten Eisenbahntunnel, Shaun, Bitzer und einem schiefen Bild – und natürlich mit der Maus, dem Elefanten und der Ente.

### 2024-06-30
Lach- und Sachgeschichten mit Jana und jeder Menge Apfelbäumen, dem Märchen vom Wunschfisch, tanzenden Schmetterlingen, Shaun und Bitzer bei der Apfel-Ernte – und natürlich mit der Maus, dem Elefanten und der Ente.

### 2024-06-23
Lach- und Sachgeschichten mit ganz besonderem Zaubersand, einer Party am Wasserloch, Ameisenlöwen und geheimnisvollen Sandlöchern, Shaun, Bitzer und einer Spinne und natürlich mit der Maus, dem Elefanten und der Ente.

### 2024-06-16 - Rasen
Lach- und Sachgeschichten mit Christoph im Fußballstadion, einem Fußball-Musical mit singenden Grashalmen, André, der immer geradeaus läuft, Shaun, Timmy und einer unvergesslichen Geburtstagsfeier und natürlich mit der Maus, dem Elefanten und der Ente.


### 2024-06-09
Lach- und Sachgeschichten in dieser Woche mit Armin und dem Geheimnis des Flugzeug-Flügels, einem Lied über einen Hund mit Propeller, einer Flugzeugdrohne im Einsatz, Shaun und einer großen Schlammschlacht – und natürlich mit der Maus, dem Elefanten und der Ente.

### 2024-06-02
Lach- und Sachgeschichten in dieser Woche mit Jana und einem besonderen Kraftstoff, der rollenden und rauchenden Bimmelbahn Henriette, einer Tankstelle für Lokomotiven, Shaun, Bitzer und einem nagelneuen Traktor und natürlich mit der Maus, dem Elefanten und der Ente.

### 2024-05-26
Lach- und Sachgeschichten mit den Geheimnissen des Longboards, dem Märchen vom verlogenen Riesen, sehr starken Pflanzen, dem winzigen Käpt’n Blaubär – und natürlich mit der Maus, dem Elefanten und der Ente.

### 2024-05-19
Lach- und Sachgeschichten in dieser Woche mit André beim Wassersport, wirklich widerspenstigen Haaren, mit Shauns neuem Kopfschmuck - und natürlich mit der Maus, dem Elefanten und der Ente.

### 2024-05-12
Lach- und Sachgeschichten in dieser Woche mit ganz vielen Regentropfen, Wanda und der Kraft der Gedanken, einem Pinsel für Armin, Shaun als Skater – und natürlich mit der Maus, dem Elefanten und der Ente.

5
Lach- und Sachgeschichten: mit mit dem Geheimnis der Ketten-Herstellung, Trudes Tier und einer tierischen Erkältung, Christoph und einem besonderen Bienen-Experiment, Shaun, Bitzer und einem Klavier auf Abwegen und natürlich mit der Maus, dem Elefanten und der Ente.

28
Lach- und Sachgeschichten: mit Andrés Tutorial zum Steine hüpfen, dem wunderbarsten Platz auf der Welt, einem eigenen Feld für Feldhamster, Käpt’n Blaubär und einem unsichtbaren Hamster – und natürlich mit der Maus, dem Elefanten und der Ente

### 2024-04-21
Lach- und Sachgeschichten in dieser Woche mit einer Orgel und jeder Menge Pfeifen, Lehrer Linke, Kati und einer kaputten Ampel, Jana und einer schweren Polizei-Ausrüstung, Shaun und einem Häftling im Schafskostüm und natürlich mit der Maus, dem Elefanten und der Ente.

### 2024-04-14
Lach- und Sachgeschichten in dieser Woche mit Siham und klebrigen Löwenzahn-Wurzeln, Pflanzenzüchter Fred und einem riesigen Feld aus Pusteblumen, Bauer Rudi und einer ganz besonderen Ernte-Maschine, schwarzem Teig aus Kautschuk – und natürlich mit der Maus, dem Elefanten und der Ente.

### 2024-04-07
Lach- und Sachgeschichten in dieser Woche mit vielen neuen Bäumen, Rico, Oskar und einem Blumenräuber, Ralph und dem Geheimnis des Bewegungsmelders, Shaun als Bitzer – und natürlich mit der Maus, dem Elefanten und der Ente.

### 2024-03-31
Lach- und Sachgeschichten diese Woche mit Ralph und dem Geheimnis vom Eiertitschen, dem Märchen vom armen Gänsemädchen, dem bekannten Lied „Bruder Jakob“ auf ganz vielen Sprachen, Shaun als Küken-Mama – und natürlich mit der Maus, dem Elefanten und der Ente.

### 2024-03-24
Lach- und Sachgeschichten in dieser Woche mit Erdkröten auf einer Wanderung, dem Märchen von einem sehr freundlichen Frosch, dem Geheimnis, ob Fische hören können, Shaun und einem Wasserrohrbruch auf der Farm – und natürlich mit der Maus, dem Elefanten und der Ente.

### 2024-03-17
Lach- und Sachgeschichten in dieser Woche mit dem Geheimnis von Ebbe und Flut, Lola beim Angeln, Johannes im Erste-Hilfe-Kurs, Käpt’n Blaubärs Geschichte vom achten Weltmeer – und natürlich mit der Maus, dem Elefanten und der Ente.

### 2024-03-10
Lach- und Sachgeschichten in dieser Woche mit Jana beim Apfel-Anbau, einer neuen Couch inklusive Mitbewohner, dem Geheimnis der Marienkäfer-Flügel, Shaun, Bitzer und dem verzauberten Farmer – und natürlich mit der Maus, dem Elefanten und der Ente.

### 2024-03-03
Lach- und Sachgeschichten in dieser Woche mit Christoph und dem Geheimnis von Zauberstiften, Rico, Oskar und einem hartnäckigen Fleck, vielen kleinen Rotkehlchen-Küken und ihrem Geschäft, Shaun und Bitzer beim Tapezieren – und natürlich mit der Maus, dem Elefanten und der Ente.

### 2024-02-25
Lach- und Sachgeschichten diese Woche mit Mona und ihrer seltenen Krankheit, Turnübungen in der Sporthalle, einem besonderen Fahrrad, Monas „Superkraftanzug“, einer rasanten Fahrt beim Rollstuhl-Skating – und natürlich mit der Maus, dem Elefanten und der Ente.

### 2024-02-18
Lach- und Sachgeschichten in dieser Woche mit Siham und dem Rätsel der Schuhgrößen, dem Märchen vom Sohn des Handschuhmachers, Armin und einer Schusterkugel, einer gemeinen Vertretung für Hofhund Bitzer und natürlich mit der Maus, dem Elefanten und der Ente.

### 2024-02-11
Lach- und Sachgeschichten diese Woche mit dem Geheimnis der alemannischen Fasnet, einem König auf der Suche nach der großen Liebe, Christoph und einem alten Kartenspiel, Hein Blöd beim Märchen-Quiz – und natürlich mit der Maus, dem Elefanten und der Ente.

### 2024-02-04
Lach- und Sachgeschichten in dieser Woche mit Christoph und einer Eisbärfigur, Lars und einem geheimnisvollen Ei, liegenden Skispringer:innen, Shaun und dem verlorenen Fußball und natürlich mit der Maus, dem Elefanten und der Ente.

### 2024-01-28
Lach- und Sachgeschichten in dieser Woche mit Malin und einer neuen Eisfläche im Eisstadion, Lola als Eisläuferin, Christoph und einem zugefrorenen See, Shaun, Bitzer und Winter-Spaß auf der Farm – und natürlich mit der Maus, dem Elefanten und der Ente.

### 2024-01-21
Lach- und Sachgeschichten in dieser Woche mit Armin und einem Hemd aus Holz, dem Märchen von dem Jungen aus Holz, Laura und einem leuchtenden Regenschirm, Shaun und einem undichten Dach – und natürlich mit der Maus, dem Elefanten und der Ente.

### 2024-01-14
Lach- und Sachgeschichten in dieser Woche mit Siham und dem Geheimnis vom Drehwurm, dem Rätsel vom erfrorenen Waldemar, einem ganz besonderen Eiskunstlauf-Training, Shaun und einer besonderen Superkraft – und natürlich mit der Maus, dem Elefanten und der Ente.

### 2024-01-07
Lach- und Sachgeschichten in dieser Woche mit Clarissa, Ralph und einem CO2-Modell, Lars im tobenden Eissturm, Armin auf der Suche nach dem Polarstern, Shaun, Bitzer und außerirdischem Besuch – und natürlich mit der Maus, dem Elefanten und der Ente.

### 2023-12-31
Lach- und Sachgeschichten in dieser Woche mit Johannes und einem ganz besonderen Glücksbringer, unterwegs mit Trude, Thorsten und dem Tier, Christoph und dem Geheimnis der Möhren-Rose, Shauns spitzen Silvester-Sause und natürlich mit der Maus und dem Elefanten. Guten Rutsch!

### 2023-12-24
Lach- und Sachgeschichten in dieser Woche mit Nulli, Priesemut und der Suche nach einem Weihnachtsbaum, dem Geheimnis von Printen, Rico, Oskar und einem Weihnachtswunder, Käpt’n Blaubär als Osterhase – und natürlich mit der Maus, dem Elefanten und der Ente.

### 2023-12-17
Lach- und Sachgeschichten diese Woche mit Armin und einer Advents-Kerze, Rico und Oskar beim Krippenspiel, dem Geheimnis der Sternenzacken, Shaun, der nicht schlafen kann – und natürlich mit der Maus, dem Elefanten und der Ente.

### 2023-12-10
Lach- und Sachgeschichten in dieser Woche mit einem wichtigen Paket auf Reisen, Rico und Oskar auf der Suche nach einem Tannenbaum, einem Weihnachtsgeschenk für Nils, Shaun, Bitzer und einer neuen Brille für den Farmer – und natürlich mit der Maus, dem Elefanten und der Ente.

### 2023-12-03
Lach- und Sachgeschichten diese Woche mit Clarissa und ganz vielen Schneekugeln, Rico, Oskar und einem Taschen-Dieb, Farbe für den Rentier-Schlitten, Shaun, Bitzer und einer schicken Hundehütte – und natürlich mit der Maus, dem Elefanten und der Ente.

### 2023-11-26
Lach- und Sachgeschichten in dieser Woche mit Siham und der Geschichte vom Kölner Dom, einer Verabredung im Himmel, Laura auf einem Schlitten ohne Schnee, Shaun und einer leckeren Pizza – und natürlich mit der Maus, dem Elefanten und der Ente.

### 2023-11-19
Lach- und Sachgeschichten in dieser Woche mit der Brückenhochzeit der neuen Leverkusener Autobahnbrücke, Herrn Ribbeck von Ribbeck im Havelland, Christoph und dem Geheimnis vom Blutdruck-Messen, Bitzer und den Schafen als Filmstars – und natürlich mit der Maus, dem Elefanten und der Ente.

### 2023-11-05
Lach- und Sachgeschichten in dieser Woche mit einem Fahrradhelm für ein Ei, Franz von Hahn, Johnny Mauser und dem dicken Waldemar mit einem kleinen Außerirdischen, Läusen unter der Lupe, Shaun, Bitzer und dem ungebetenem Besuch – und natürlich mit der Maus, dem Elefanten und der Ente.

### 2023-10-29
Lach- und Sachgeschichten in dieser Woche mit dem Geheimnis vom Spinnennetz, Charlie, Lola und ihrer Hausspinne, einer großen Portion Ekel, Shaun und Bitzer als Kürbis-Wächter – und natürlich mit der Maus, dem Elefanten und der Ente.

### 2023-10-22
Lach- und Sachgeschichten in dieser Woche mit André und dem Geheimnis der Windenergie, einem stürmischen Herbst-Lied, der Suche nach dem Tier des Jahres, Shaun auf der Jagd nach dem Drachen – und natürlich mit der Maus, dem Elefanten und der Ente.

### 2023-10-15
Lach- und Sachgeschichten diese Woche mit Johannes auf einem Pferd, Trudes Tier und einem sehr protzigen Geweih, dem Geheimnis vom Hirsch-Geweih, einem Wettbewerb für Hofhund Bitzer – und natürlich mit der Maus, dem Elefanten und der Ente.

### 2023-10-08
Lach- und Sachgeschichten in dieser Woche mit Christoph und ganz viel Laub, dem Märchen Der Hase und der Igel, dem Geheimnis der Igel-Stacheln, Schäfchen Timmy und einem Winter-Versteck für die Igel – und natürlich mit der Maus, dem Elefanten und der Ente.

### 2023-10-01
Lach- und Sachgeschichten in dieser Woche mit André auf einem ganz besonderen Schiff, dem fliegenden Jakob, mit einer Delle im Flugzeug, Käpt’n Blaubärs Hollywood-Eskapaden und mit der Maus und dem Elefanten.

### 2023-09-24
Lach- und Sachgeschichten diese Woche mit Siham und sehr großen Tieren, Trude und dem Tier als Internetstars, einem tierisch guten Bagger-Ballett, Shaun und den Farm-Tieren im Megastress – und natürlich mit der Maus und dem Elefanten.

### 2023-09-17
Lach- und Sachgeschichten diese Woche heute mit Laura auf dem Rhein, Krawinkel auf der Riesenwelle, Kisten mit ganz viel Müll, einem wildgewordenen Staubsauger und mit der Maus und dem Elefanten.

### 2023-09-10
Lach- und Sachgeschichten mit vielen bunten Schulstühlen, Streit auf dem Schulhof, dem Geheimnis vom viereckigen Spinat, Shaun, dem Super-Schaf – und natürlich mit der Maus und dem Elefanten.

### 2023-09-03


### 2023-08-27


### 2023-08-20


### 2023-08-13


### 2023-08-06
Lach- und Sachgeschichten in dieser Woche mit André und ganz vielen Blubber-Blasen, dem Tier beim Training, einem sensationellen Fußball-Selfie, Bitzer als Torwart und mit der Maus, dem Elefanten und der Ente.

### 2023-07-30
Lach- und Sachgeschichten in dieser Woche mit Laura, Christoph und einem knattrigen Boot, Rico, Oskar und einer amtlichen Arschbombe, einem glühenden Metallpfannkuchen, Hein und seiner Mond-Apfelsine und natürlich mit der Maus und dem Elefanten.

### 2023-07-23
Lach- und Sachgeschichten in dieser Woche mit Siham bei einem Solarkraftwerk mitten in der Wüste, einem sehr sonnigen Tag, dem Geheimnis der Herstellung von Arganöl, Shaun als "weißer Hai" und natürlich mit der Maus, dem Elefanten und der Ente.

### 2023-07-16
Lach- und Sachgeschichten in dieser Woche mit Siham in einer besonderen marokkanischen Schule, Lehrer Linke und einer Traumreise, dem Geheimnis vom Töpfern, Shaun als Eisverkäufer und natürlich mit der Maus, dem Elefanten und der Ente.

### 2023-07-09
Lach- und Sachgeschichten in dieser Woche mit Sihams neuen Pantoffeln aus Marokko, Kalle im Urlaub, einer ganz besonderen Schrift, einer gefräßigen Ziege und natürlich mit der Maus, dem Elefanten und der Ente.

### 2023-07-02
Lach- und Sachgeschichten in dieser Woche mit Sihams Sommerreise nach Marokko, einem Lied vom Sommer, ganz besonderen, marokkanischen Lederpantoffeln, neuen Turnschuhen für den Farmer – und natürlich mit der Maus, dem Elefanten und der Ente.

### 2023-06-25 Goldwaschen Rhein Glitzer
Lach- und Sachgeschichten in dieser Woche mit dem Geheimnis der Wendepailletten-Shirts, Lola beim Schul-Fotografen, André und einem Spaghetti-Experiment, Hein und einem ganz besonderen Würfel – und natürlich mit der Maus, dem Elefanten und der Ente.

### 2023-06-18 Wasser Regen Schnee
Lach- und Sachgeschichten in dieser Woche mit einem schnittigen Wasserstrahl, Nulli, Priesemut und einem wirbeligen Wetterfrosch, Siham im Regen, Camping-Chaos auf der Farm und natürlich mit der Maus und dem Elefanten.

### 2023-06-11
Lach- und Sachgeschichten in dieser Woche mit Christoph und einem verflixten Fleck, Willi und seinem sehr erwachsischen Papa, dem Geheimnis vom Seifenspender, einer Waschstraße für den Farmer und natürlich mit der Maus und dem Elefanten.

### 2023-06-04
Lach- und Sachgeschichten in dieser Woche mit einem feurigen Salamander, dem schluckigen Schluckster, Armin und ganz vielen gummrigen Bärchen, dem sehr fitten Farmerund natürlich mit der Maus und dem Elefanten.

### 2023-05-28
Lach- und Sachgeschichten mit Armin und den Seilen an der Autobahnbrücke in Leverkusen, Zwillingen bei einem Zwist im Wald, André, der auf einen Baum klettert, Shaun, der einen Baum rettet und natürlich mit der Maus und dem Elefanten.

### 2023-05-21
Lach- und Sachgeschichten in dieser Woche mit einem artigen Artenschutzhund, Rico, Oskar und einem kuscheligen Köter, sehr müden Fohlen, Bitzer dem Briefträger-Schreck und natürlich mit der Maus und dem Elefanten.

### 2023-05-14
Lach- und Sachgeschichten in dieser Woche mit buddelnden Wildbienen, Trudes Mutter, die einen Kuchen probiert, Christoph, der einen Kuchen backt, einem neuen Hut für Bitzer und mit der Maus, dem Elefanten und der Ente.

### 2023-05-07 (s)
Lach- und Sachgeschichten in dieser Woche mit Smudos famoser Stein-Flitsch-Maschine, Nulli, Priesemut und einem flotten Flitzeboot, Christoph und einem kaputten Kutter, dem Holzblaskünstler Käpt’n Blaubär und natürlich mit der Maus und dem Elefanten.

### 2023-04-30
Lach- und Sachgeschichten in dieser Woche mit Siham zu Besuch bei fleischfressenden Pflanzen, drei Freunden und der Biene Sabine, vielen fleißigen Bienen in der Stadt, einem summenden Störenfried und natürlich mit der Maus und dem Elefanten.

### 2023-04-23
Lach- und Sachgeschichten in dieser Woche mit Armin und dem Geheimnis der Wochentage, Max und dem Geheimnis der Rolltreppe, Jana und dem Lösen des Geheimnisses der Rolltreppe und Hein Blöd, der gar nichts löst und mit der Maus und dem Elefanten.

### 2023-04-16
Lach- und Sachgeschichten in dieser Woche mit einer rasenden Rohr-Post, einem rennenden Ritter Rost, ner richtigen Riesenbeule, einem reizenden Röstfisch und mit der Maus, dem Elefanten und der Ente.

### 2023-04-09
Lach- und Sachgeschichten in dieser Woche mit André und ganz besonderen Bodyguards, einem Geschenk für den Osterhasen, tanzenden Hühnern, einem Wettkampf auf allerhöchstem Niveau und natürlich mit der Maus und dem Elefanten.

### 2023-04-02
Lach- und Sachgeschichten in dieser Woche mit Christophs Karussellmodell, Lolas Kirmesbesuch, Lauras Zuckerwattemaschine, Shauns Kokosnussknacker und natürlich mit der Maus und dem Elefanten.

### 2023-03-26
Lach- und Sachgeschichten mit Siham im Haifischbecken, einer sehr ernsten Prinzessin, einer Klebepremiere am Flugzeug, ‘ner ungewöhnlichen Antennenreparatur und mit der Maus und dem Elefanten.

### 2023-03-19 - Gummistiefel
Lach- und Sachgeschichten in dieser Woche mit ganz jungen Frischlingen, einem verliebten Frosch, Füßen aus Metall, einem geheimen Party-Versteck und mit der Maus und dem Elefanten.

### 2023-03-12
Lach- und Sachgeschichten in dieser Woche mit einem sehr luftigen Arbeitsplatz, singenden Feuerwehrleuten, Felix und Johannes bei einer Löschübung, Käpt’n Blaubär und seiner fleischfressenden Pflanze und natürlich mit der Maus und dem Elefanten.

### 2023-03-05x
Lach- und Sachgeschichten in dieser Woche mit jeder Menge Puffreis, jeder Menge Neuschnee, dem Geheimnis vom Parkpieper, Shaun auf Schatzsuche und natürlich mit der Maus und dem Elefanten.

### 2023-02-26
Lach- und Sachgeschichten in dieser Woche mit Armin und dem Trick mit den Papiertaschentüchern, der kranken Lola, Christophs Besuch bei Noah und Sophia, den Schafen im Puzzlefieber und natürlich mit der Maus und dem Elefanten.

### 2023-02-19
Lach- und Sachgeschichten in dieser Woche mit wichtiger Maßarbeit, Krawinkel als Ritter, Armin als Ritter, Käpt’n Blaubär auf der Karnevalsinsel und mit der Maus, dem Elefanten und der Ente.

### 2023-02-12
Lach- und Sachgeschichten in dieser Woche mit Antworten von Kindern, einer Frage des Geschmacks, Antworten von Erwachsenen, Streit um einen Kohlkopf – äh, Fußball und mit der Maus, dem Elefanten und der Ente.

### 2023-02-05
Lach- und Sachgeschichten in dieser Woche mit einem besonderen Knoten im Stahlseil, zwei sehr guten Freunden, Schnee für Pinguine, der Geschichte der Ponguine und natürlich mit der Maus, dem Elefanten und der Ente.

### 2023-01-29 Kuchen
Lach- und Sachgeschichten in dieser Woche mit einem besonderen Schmuckstück, einem Prinzen beim Verstecken spielen, einer eisigen Erfindung, einem schafen Fotomotiv und mit der Maus, dem Elefanten und der Ente.


### 2023-01-22?
Lach- und Sachgeschichten mit Häuschen für die neue Leverkusener Autobahnbrücke, Krawinkels Stöckchenpuzzle, einer sehr besonderen Gitarre, dem liebsten Opi aller Zeiten und natürlich mit der Maus und dem Elefanten.

### 2023-01-15
Lach- und Sachgeschichten in dieser Woche mit einem aufgehenden Hefeteig, Willi Wiberg auf der Suche nach Ideen gegen Langeweile, Armin und dem Geheimnis vom Beipackzettelfalten, Käpt’n Blaubär als Facharzt für Bärologie und natürlich mit der Maus, dem Elefanten und der Ente.

### 2023-01-08 Kakao
Lach- und Sachgeschichten in dieser Woche mit einem aufgehenden Hefeteig, Willi Wiberg auf der Suche nach Ideen gegen Langeweile, Armin und dem Geheimnis vom Beipackzettelfalten, Käpt’n Blaubär als Facharzt für Bärologie und natürlich mit der Maus, dem Elefanten und der Ente.

### 2023-01-01 Schokolade
Lach- und Sachgeschichten in dieser Woche mit einem Schokoladenexperiment, einer Eiszeit im Schnee, Christoph und Johannes bei ‘ner Kissenschlacht, Shaun und Bitzer als Traumpaar und mit der Maus und dem Elefanten.

### 2022-12-25 Koenig Nussknacker - 2022 Jahresruckblick
Lach- und Sachgeschichten in dieser Woche mit Siham und einer sehr schönen Aussicht, Warten auf den ersten Schnee, mit kunstvollen Holzarbeiten, mit noch einer sehr schönen Aussicht – und natürlich mit der Maus, dem Elefanten und der Ente.

### 2022-12-18
Lach- und Sachgeschichten in dieser Woche mit jeder Menge Weihnachtsgeschenkpapier, einer schönen Bescherung in Mullewapp, ganz viel Handarbeit, noch mehr Handarbeit und mit der Maus und dem Elefanten

### 2022-12-11 Glocke
Lach- und Sachgeschichten in dieser Woche mit wichtigen Weihnachtsvorbereitungen, dem kleinen Maulwurf und sehr musikalischen Tieren, einem heiß-kalten Trick, Shaun als Meisterdetektiv und natürlich mit der Maus und dem Elefanten.

### 2022-12-04 
Lach- und Sachgeschichten in dieser Woche mit einem ganz besonderen Nussknacker, der Geschichte vom Nikolaus, Christophs Adventskalender, einem superschnellen Nussräuber und natürlich mit der Maus und dem Elefanten.

### 2022-11-27 weißer Schokolade schoene Maus
Lach- und Sachgeschichten in dieser Woche mit dem Geheimnis des Unterschieds von brauner und weißer Schokolade, Otto in der Weihnachtsbäckerei, Lauras Weihnachtsdruckerei und Käpt’n Blaubär als leuchtendem Beispiel.

### 2022-11-20 - Laut Schlafen Verrückte Maschine 
Lach- und Sachgeschichten in dieser Woche mit Armins verrückter Maschine, einem zu lauten Auto, einem Chor an einer sehr lauten Straße, Shauns Schafschlafstörung und natürlich mit der Maus und dem Elefanten.

### 2022-11-13 - St. Martin 3D Druck Haus Weckmann Becker
Lach- und Sachgeschichten diese Woche mit einem Haus aus dem Drucker, einem Haus aus Kuchen, einer großen Rolle aus Teig, Heins Blödsinn und mit der Maus und dem Elefanten.

### 2022-11-06 - GG Schutzschild Grundgesetzgirl Stunt Schubsen
Lach- und Sachgeschichten in dieser Woche mit einem ganz besonderen Schutzschild, einem Tag im Leben von Grundgesetzgirl, einem echten Fahrrad-Stunt, außergewöhnlichen Fähigkeiten und mit der Maus und dem Elefanten.

### 2022-10-30 - Chlorophyl Gruen Gelb Herbst Badewanne Halloween 3D Kaktus Glas Wasservb 
Lach- und Sachgeschichten in dieser Woche mit einem Park mit Badewanne, mit einem gespenstischen Pullover, einem Glas Wasser, mit Bitzer als Modell – und natürlich mit der Maus, dem Elefanten und der Ente.

### 2022-10-23 - Scheibenwischer Nupsies Mueffelchen Silberfische Moeheren Karotten Fruchtgummi
Lach- und Sachgeschichten in dieser Woche mit Lauras neuer Erfindung, Müffelchen in der Badewanne, Christoph bei der Möhrenernte, zu viel Fruchtgummi in Dover und mit der Maus und dem Elefanten.

### 2022-10-16
Lach- und Sachgeschichten mit einem Schäkel in klein – und in groß, einem schlauen Geißbock,einer Bahnschranke bei der Arbeit, einer unruhigen Nacht und natürlich mit der Maus, dem Elefanten und der Ente.

### 2022-10-08 - Birne
Lach- und Sachgeschichten in dieser Woche mit Christoph und dem Geheimnis vom Apfelstiel, Meeresriegeln für Lola, einem Quallen-Leibgericht, Hofverkauf auf der Farm und natürlich mit der Maus und dem Elefanten.

### 2022-10-02
Lach- und Sachgeschichten in dieser Woche mit einem künstlichen Rüssel, einer tierischen Begabung, Musik mit Robotern, Käpt’n Blaubärs Einsame-Herzen-Band und natürlich mit der Maus, dem Elefanten und der Ente.

### 2022-09-25
Lach- und Sachgeschichten mit Laura und dem Geheimnis vom Litzenheber, Krawinkels neuer Erfindung, einem großen Tag auf einer Riesenbaustelle, einem Stau vor der Farm und mit der Maus und dem Elefanten.

### 2022-09-18
Lach- und Sachgeschichten diese Woche mit einem Auto-Roboter, der Herstellung eines Autoreifens, früher und heute, einem neuen Helfer auf der Farm und mit der Maus und dem Elefanten.

### 2022-09-11
Lach- und Sachgeschichten diese Woche mit Knochen von einem Dinosaurier, Charlies Dinosaurier-Sammlung, einer Dino-Pfote, der Geschichte vom Bärosaurier und mit der Maus und dem Elefanten.

### 2022-09-04 - Haselnusscreme
Lach- und Sachgeschichten mit Christoph in einer Haselnussplantage, Kati Sommer – heute ein bisschen zu früh, dem Trick, mit den Flüssigkeiten, dem Farmer beim Angeln und natürlich mit der Maus, dem Elefanten und der Ente.

### 2022-08-28
Lach- und Sachgeschichten diese Woche mit Laura, Brindis und einer ganz besonderen Filteranlage, dem kleinen Maulwurf und einem großen Ausflug, einem Regenexperiment und natürlich mit der Maus und dem Elefanten.

### 2022-08-21
Lach- und Sachgeschichten diese Woche mit frischem Torfmoos, einem Fall für die drei Freunde, einem Dach für eine Himbeer-Plantage, Shaun als Farmer und natürlich mit der Maus und dem Elefanten.

### 2022-08-14
Lach- und Sachgeschichten diese Woche mit einem Bus, der mit Strom fährt, einer selbstgebauten Seifenkiste, einem LKW, der mit Wasserstoff fährt, Zork Blaubär, dem Erfinder des Automobils, und mit der Maus und dem Elefanten.

### 2022-08-07
Lach- und Sachgeschichten mit Ralph und Riccardo in der Zugspitze, Anke auf einer Stuhlpyramide, einer neuen Idee für Platten aus Beton, einer Baustelle auf der Farm und natürlich mit der Maus und dem Elefanten.

### 2022-07-31
Lach- und Sachgeschichten diese Woche mit einem Solarpark auf einem Baggersee, Krawinkel beim Buddeln, einem ganz besonderen Ofen, Heins Regenbogen und natürlich mit der Maus und dem Elefanten.


### 2022-07-24
Lach- und Sachgeschichten diese Woche mit Deutschlands höchstgelegener Wetterstation, Katis kaputtem Regenschirm, dem Trick, mit dem Feuer machen und natürlich mit der Maus und dem Elefanten.

### 2022-07-17
Lach- und Sachgeschichten mit einer Hülle für einen Zeppelin, einer frechen Fliege, dem Trick mit dem schwebenden Gleichgewicht, einer aufregenden Ballonfahrt und mit der Maus und dem Elefanten.

### 2022-07-10
Lach- und Sachgeschichten diese Woche mit Armin und dem Geheimnis vom Flaschenzug, Gewichtheben – heute mal anders, einem Pokal für Christoph, einem schlagkräftigen Shaun – und natürlich mit der Maus, dem Elefanten und der Ente.

### 2022-07-03
Lach- und Sachgeschichten mit einer Wiese in den Bergen, einem Sommerlied, ganz vielen Kerzen, einer Theatervorstellung für Timmy und mit der Maus und dem Elefanten.


### 2022-06-26 - Quardratmeter Natur, besonderer Hammer

### 2022-06-19 - Stockbrot, Autism, Lutzi

### 2022-06-05
Lach- und Sachgeschichten mit Siham und der Geschichte vom Aachener Dom, Henrys Mutprobe, Herrn Granow und seinem Roboterbagger, dem Farmer und seinen großen Plänen und natürlich mit der Maus und dem Elefanten.

00:00,01:00,02:01,10:00,11:10,13:15,15:00,20:40,21:16,23:38


